output "primary_ipv4_addresses" {
  value = google_compute_instance.primary.network_interface.0.access_config.0.nat_ip
}

output "secondary_ipv4_addresses" {
  value = google_compute_instance.secondary.network_interface.0.access_config.0.nat_ip
}

