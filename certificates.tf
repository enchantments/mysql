resource "tls_private_key" "primary" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_private_key" "secondary" {
  algorithm   = "ECDSA"
  ecdsa_curve = "P384"
}

resource "tls_cert_request" "primary" {
  key_algorithm   = "ECDSA"
  private_key_pem = tls_private_key.primary.private_key_pem

  subject {
    common_name   = "MySQL Primary"
    organization  = var.organization
    province      = var.province
    locality      = var.locality
  }

  dns_names = [
    "mysql.service.consul",
    "primary.mysql.service.consul",
    "localhost",
  ]

  ip_addresses = [
    "127.0.0.1"
  ]
}

resource "tls_cert_request" "secondary" {
  key_algorithm   = "ECDSA"
  private_key_pem = tls_private_key.secondary.private_key_pem

  subject {
    common_name   = "MySQL Secondary"
    organization  = var.organization
    province      = var.province
    locality      = var.locality
  }

  dns_names = [
    "mysql.service.consul",
    "secondary.mysql.service.consul",
    "localhost",
  ]

  ip_addresses = [
    "127.0.0.1"
  ]
}

resource "tls_locally_signed_cert" "primary" {
  cert_request_pem      = tls_cert_request.primary.cert_request_pem
  ca_private_key_pem    = var.intermediate_private_key_pem
  ca_cert_pem           = var.intermediate_certificate_pem
  ca_key_algorithm      = "ECDSA"

  validity_period_hours = 72

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth",
  ]
}

resource "tls_locally_signed_cert" "secondary" {
  cert_request_pem      = tls_cert_request.secondary.cert_request_pem
  ca_private_key_pem    = var.intermediate_private_key_pem
  ca_cert_pem           = var.intermediate_certificate_pem
  ca_key_algorithm      = "ECDSA"

  validity_period_hours = 72

  allowed_uses = [
    "key_encipherment",
    "digital_signature",
    "server_auth",
    "client_auth",
  ]
}
