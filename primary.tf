resource "google_compute_instance" "primary" {

  name         = "mysql-primary"
  machine_type = "n1-standard-1"
  project      = var.project
  zone         = var.zone
  
  boot_disk {
    initialize_params {
      image = "ubuntu-os-cloud/ubuntu-1804-lts"
    }
  }

  network_interface {
    network = "default"

    access_config {
    }
  }

  service_account {
    scopes = ["userinfo-email", "compute-ro", "storage-ro"]
  }

  connection {
    type        = "ssh"
    host        = self.network_interface.0.access_config.0.nat_ip
    user        = var.ssh_user
    private_key = file(var.ssh_private_key)
  }

  provisioner "remote-exec" {
    inline = [
      "mkdir -p /tmp/tls",
      "sudo mkdir -p /etc/tls /etc/consul",
    ]
  }

  provisioner "file" {
    destination = "/tmp/tls/key.pem"
    content     = tls_private_key.primary.private_key_pem
  }

  provisioner "file" {
    destination = "/tmp/tls/cert.pem"
    content     = tls_locally_signed_cert.primary.cert_pem
  }

  provisioner "file" {
    destination = "/tmp/tls/ca.pem"
    content     = var.intermediate_certificate_pem
  }

  provisioner "file" {
    destination = "/tmp"
    source      = "${path.module}/files"
  }

  provisioner "file" {
    destination  = "/tmp/files/consul/datacenter.json"
    content      = templatefile("${path.module}/templates/consul/datacenter.json", {
      datacenter = var.datacenter
    })
  }

  provisioner "file" {
    destination  = "/tmp/files/consul/gossip.json"
    content      = templatefile("${path.module}/templates/consul/gossip.json", {
      gossip_key = var.control_plane_consul_gossip_key
    })
  }

  provisioner "file" {
    destination  = "/tmp/files/consul/retry-join.json"
    content      = templatefile("${path.module}/templates/consul/retry-join.json", {
      project    = var.project
    })
  }

  provisioner "file" {
    destination  = "/tmp/files/consul/mysql.json"
    content      = templatefile("${path.module}/templates/consul/mysql.json", {
      tag = "primary"
    })
  }

  provisioner "file" {
    destination  = "/tmp/hashicorp-product-versions.env"
    content      = join("\n", [
      "CONSUL_VERSION=${coalesce(var.consul_version, jsondecode(data.http.consul_product_info.body).current_version)}",
    ])
  }

  provisioner "remote-exec" {
    scripts = [
      "${path.module}/scripts/install-basic-software.sh",
      "${path.module}/scripts/install-consul.sh",
      "${path.module}/scripts/install-mysql.sh",
    ]
  }

  provisioner "remote-exec" {
    inline = [
      "sudo mv /tmp/tls/* /etc/tls",
      "sudo mv /tmp/files/consul/* /etc/consul",
      "sudo mv /tmp/files/systemd/* /lib/systemd/system",
      "sudo systemctl daemon-reload",
      "sudo systemctl enable consul.path",
      "sudo systemctl start consul.path",
    ]
  }
}
