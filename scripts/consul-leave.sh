#!/bin/bash

set -e

echo "Disabling consul.path"
sudo systemctl disable consul.path
echo "Disabling consul.service"
sudo systemctl disable consul.service
echo "Stopping consul.path"
sudo systemctl stop consul.path
echo "Stopping consul.service"
sudo systemctl stop consul.service
