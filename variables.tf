variable "datacenter" {}
variable "region" {}
variable "control_plane_consul_gossip_key" {}
variable "ssh_user" {}
variable "ssh_private_key" {}
variable "project" {}
variable "zone" {}
variable "intermediate_private_key_pem" {}
variable "intermediate_certificate_pem" {}
variable "root_certificate_pem" {}
variable "organization" { default = "" }
variable "province" { default = "" }
variable "locality" { default = "" }
variable "consul_version" {
  default = ""
}
