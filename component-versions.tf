data "http" "consul_product_info" {
  url = "https://checkpoint-api.hashicorp.com/v1/check/consul"

  request_headers = {
    "Accept" = "application/json"
  }
}
